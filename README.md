# wpLandoClone

Cal Evans <cal@calevans.com>

**This project assumes you already have [lando](https://lando.dev) installed and working. If not, do that first. Nothing here will work if that isn't working.**

# IMPORTANT

```bash
export COMPOSER_VENDOR_DIRECTORY=/app/public/wp-content/plugins/pressoverbum/vendor
```
or whatever plugin you are working on. Otherwise, it gets installed in /app/vendor

This project will create a Lando development environment for a WordPress site and then clone a site down from production so you can work on it. This is a **ONE WAY** process. it will not push your changes back up to production. The primary use of this project is for developers or designers to clone a production site down so they have a workspace.

This project does not deal with intricacies like license keys for the various plugins and themes you have installed. That's on you.

# Instructions

**1. Clone this repo into the directory you want your site installed in.**

```
$ git clone git@gitlab.com:calevans/wplandoclone.git example.com
```

This will place everything in a directory called example.com.

**2. `cd` into the directory you just created.**

```
$ cd example.com
```

**3. Run the `init.sh` script.**<br>
 `init.sh` takes one parameter, the name of the project. This **CAN BE** the domain name but it doesn't have to be. Whatever you want. I usually keep them short. I do not recommend using the full domain name with the top level domain. e.g. use `example` not `example.com`.

```
$ ./init.sh example
```

**4. Modify `.env`**<br>
`init.sh` created a .env file in the `scripts` directory for you. Open `scripts/.env` with your favorite text editor and fill in the needed values.

```
ORIGIN_HOST=""
ORIGIN_USERNAME=""
ORIGIN_PRIVATE_KEY="/user/.ssh/id_rsa"
ORIGIN_DOMAIN=""
ORIGIN_PATH=""
ORIGIN_PORT=22
```

- `ORIGIN_HOST` is the machine name of the server that hosts the website currently. This is USUALLY just the domain name, but there are cases where it is not.
- `ORIGIN_USERNAME` is the username you use to ssh into the server that hosts the website currently.
- `ORIGIN_PRIVATEKEY` This is the complete path and file name to the private key **INSIDE OF THE LANDO CONTAINER** you use to access the server. In most cases this is going to be `/user/.ssh/id_rsa` but check and make sure before entering a bad value.
- `ORIGIN_DOMAIN` is the actual domain name. There's a good chance that its the same as `ORIGIN_HOST`. There are hosts however where they will be different.
- `ORIGIN_PATH` This is the directory on the host where your website is stored. This is called a couple different things by different hosts, **ROOT_DIR**, or **APPLICATION_PATH** are other terms used to mean the same thing. This one can be tricky. The easiest way I've found to determine this  is to ssh into the server, go to the root directory of your website and enter `pwd`. Whatever that is, is **probably** the `ORIGIN_PATH`. If not, you may have to tinker a bit to get it.
- `ORIGIN_PORT` This is the port that SSH runs on on the server `ORIGIN_HOST`. For most servers, this is going to be 22. However, there are some hosts that run it on a non-standard port. If your host is one of those, change this number.

**5. Review `.lando.yml`**<br>
Take time to review the `.lando.yml` file that was generated.

- Is the PHP version what you want?
- Do you want mariadb instead of mysql?
- Are there other packages you want installed as it's being built?

Look it over and make sure everything is what you want. If not, change it, it's yours to do with as you like.

**5. Run `lando start`**<br>
Now start lando. it will create a default WordPress site and then clone your site into this shell. That's all it takes. When it's done, lando will give you the urls to go to to see your new sites. If you've never used lado before, be aware that when you go to the `https` version of your site, most browsers will give you a warning and ask you to confirm that you really want to go to this site. it's perfectly safe, go ahead and confirm. This should take you to your site.

```
$ lando start
```

**6. Go forth and create awesome.**

# Re-Clone

There may come a time  when you need to re-clone the production database.  To ddo this use the command `lando clonewp` it will re-run the cloning process.

```
$ lando clonewp
```

**WARNING! WARNING! WARNING!**

If you re-clone it will do so indiscriminately.

- It will completely erase your database and load the current production one.
- It will also copy down all files,l erasing any files you may have modified.

You have been warned.

# mailhog

By default this will also install and spin up `mailhog`. This intercepts all email and gives you a web interface to look at it. You don't have to change all your email addresses to dummy email addresses. I find this to be invaluable when working on a site.