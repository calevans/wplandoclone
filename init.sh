#!/bin/bash
#
# $1 is the name of the project
# If you don't want the root dir of your site in public_hmtl then change this.
#  WordPress will uncompress into wordpress. We move this to whatever $ROOT_DIR
# is.
PROJECT_NAME=$1
ROOT_DIR=public_html
source scripts/helper_functions.sh
#
# Make sure we have everything we need to succeed.
#
if [ -z $PROJECT_NAME ]
then
  red_text "You have to specify a project name for this script to work properly."
  exit 1
fi

if [ -z $(which perl) ]
then
  red_text "This script relies on perl to create the .lando.yml file. I don't"
  red_text "detect perl installed."
  exit 2
fi

if [ ! -f scripts/lando_yaml.template ]
then
  red_text "I can't find the lando.yml template file. Make sure you cloned the repository "
  red_text "https://gitlab.com/calevans/wplandoclone and then try again."
  exit 3
fi

#
# Init Lando for this project
#
lando init \
  --source remote \
  --remote-url https://wordpress.org/latest.tar.gz \
  --recipe wordpress \
  --webroot $ROOT_DIR \
  --name $PROJECT_NAME

mv wordpress $ROOT_DIR

#
# Create a .lando.yml file for the project
#
cp scripts/lando_yaml.template ./.lando.yml
perl -p -i -e "s/{{PROJECT_NAME}}/$PROJECT_NAME/g" ./.lando.yml
perl -p -i -e "s/{{ROOT_DIR}}/$ROOT_DIR/g" ./.lando.yml

#
# Create an empty .env file
#
cp scripts/.env.sample scripts/.env

#
# These files belong to wpclonelando, not the project being cloned. :)
#
rm CODE_OF_CONDUCT.md
rm LICENSE.md

echo
green_text "You are almost there!"
echo
green_text "Before you can successfully clone an existing site, you need to fill out"
green_text "the scripts/.env file. Open it in your favorite text editor and give"
green_text "proper values to all the pieces of information there. Usually the hardest"
green_text  "is the key file. This is the full path and name of the private key that"
green_text "you use to access the server that the website hosted on."
echo
green_text "Once you've done that, then run lando start to create the docker environment"
green_text "and clone the site."
echo
green_text "You can now delete init.sh as you never need to run it again for this project."